package de.naturzukunft.rdf.solid.pod.adapter;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.web.reactive.function.client.ExchangeFilterFunction;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.ClientResponse.Headers;

import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;
import reactor.netty.http.client.HttpClient;

@Configuration
@Slf4j
public class WebClientConfig {

	@Bean
	public WebClient.Builder webClientBuilder() {
		return WebClient.builder();
	}

	@Bean
	public HttpClient httpClient() {
		return HttpClient.create();
	}
	
	@Bean
	public WebClient webClient() {
		
		return webClientBuilder()
				.filters(exchangeFilterFunctions -> {
				      exchangeFilterFunctions.add(logRequest());
				      exchangeFilterFunctions.add(logResponse());
				  })
				.clientConnector(new ReactorClientHttpConnector(httpClient()))
				.build();
	}
	
	private ExchangeFilterFunction logRequest() {
	    return ExchangeFilterFunction.ofRequestProcessor(clientRequest -> {
	        if (log.isDebugEnabled()) {
	            StringBuilder sb = new StringBuilder("Request: \n");
	            sb.append(clientRequest.method()).append("\n");
	            sb.append(clientRequest.url()).append("\n");
	            clientRequest
	              .headers()
	              .forEach((name, values) -> {
	            	  sb.append("Header: " + name + " - " + values).append("\n");
	              }
	    		);
	            log.debug(sb.toString());
	        }
	        return Mono.just(clientRequest);
	    });
	}
	
	private ExchangeFilterFunction logResponse() {
	    return ExchangeFilterFunction.ofResponseProcessor(clientResponse -> {
	        if (log.isDebugEnabled()) {
	            StringBuilder sb = new StringBuilder("Response: \n");
	            //append clientRequest method and url
	            Headers headers = clientResponse.headers();
	            headers.asHttpHeaders().forEach((name, values) -> values.forEach(value -> log.debug( "-"+ value)));
	            log.debug(sb.toString());
	        }
	        return Mono.just(clientResponse);
	    });
	}
}
