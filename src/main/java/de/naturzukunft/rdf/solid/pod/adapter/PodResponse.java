package de.naturzukunft.rdf.solid.pod.adapter;

import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;

import lombok.Data;

@Data
public class PodResponse {

	private HttpStatus statusCode;
	private MultiValueMap<String, String> headers;
	private String body;

	public PodResponse(HttpStatus statusCode, MultiValueMap<String, String> headers, String body) {
		this.statusCode = statusCode;
		this.headers = headers;
		this.body = body;
	}

	public String toString() {
		StringBuilder sb = new StringBuilder("statusCode: ");
		sb.append(statusCode).append("\n");
		headers.forEach((k,v) ->  sb.append("header: " + k + " - " + v + "\n") );
		sb.append("body: ").append(body);
		return sb.toString();
	}
}
