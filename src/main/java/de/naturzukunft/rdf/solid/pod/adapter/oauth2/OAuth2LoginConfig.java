package de.naturzukunft.rdf.solid.pod.adapter.oauth2;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.security.oauth2.client.endpoint.DefaultAuthorizationCodeTokenResponseClient;
import org.springframework.security.oauth2.client.endpoint.OAuth2AccessTokenResponseClient;
import org.springframework.security.oauth2.client.endpoint.OAuth2AuthorizationCodeGrantRequest;
import org.springframework.security.oauth2.client.http.OAuth2ErrorResponseErrorHandler;
import org.springframework.security.oauth2.client.registration.ClientRegistration;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.security.oauth2.client.registration.InMemoryClientRegistrationRepository;
import org.springframework.security.oauth2.core.AuthorizationGrantType;
import org.springframework.security.oauth2.core.ClientAuthenticationMethod;
import org.springframework.security.oauth2.core.http.converter.OAuth2AccessTokenResponseHttpMessageConverter;
import org.springframework.security.oauth2.core.oidc.IdTokenClaimNames;
import org.springframework.web.client.RestTemplate;


@Configuration
public class OAuth2LoginConfig {

	@Autowired
	private CustomRequestEntityConverter customRequestEntityConverter;  
	
	@Bean
    public OAuth2AccessTokenResponseClient<OAuth2AuthorizationCodeGrantRequest> accessTokenResponseClient(){
        DefaultAuthorizationCodeTokenResponseClient accessTokenResponseClient = 
          new DefaultAuthorizationCodeTokenResponseClient(); 
        accessTokenResponseClient.setRequestEntityConverter(customRequestEntityConverter); 
 
        OAuth2AccessTokenResponseHttpMessageConverter tokenResponseHttpMessageConverter = 
          new OAuth2AccessTokenResponseHttpMessageConverter(); 
        RestTemplate restTemplate = new RestTemplate(Arrays.asList(
          new FormHttpMessageConverter(), tokenResponseHttpMessageConverter)); 
        restTemplate.setErrorHandler(new OAuth2ErrorResponseErrorHandler()); 
        
        accessTokenResponseClient.setRestOperations(restTemplate); 
        return accessTokenResponseClient;
    }
	
	@Bean
	public ClientRegistrationRepository clientRegistrationRepository() {
		return new InMemoryClientRegistrationRepository(List.of(this.solidCOmmunityClientRegistration(), this.inruptClientRegistration()));
	}

	private ClientRegistration solidCOmmunityClientRegistration() {
		return ClientRegistration.withRegistrationId("solidcommunity")
			.clientId("3d542a42bf3ccfbbe016871ebfd1b1bb")
			.clientSecret("96eedcffd92525695488e2d926f6158d")
			.clientAuthenticationMethod(ClientAuthenticationMethod.BASIC)
			.authorizationGrantType(AuthorizationGrantType.AUTHORIZATION_CODE)
			.redirectUriTemplate("{baseUrl}/login/oauth2/code/{registrationId}")
			.scope("openid", "read", "write", "append", "control")
			.authorizationUri("https://solidcommunity.net/authorize")
			.tokenUri("https://solidcommunity.net/token")
			.userNameAttributeName(IdTokenClaimNames.SUB)
			.jwkSetUri("https://solidcommunity.net/jwks")
			.clientName("solidcommunity")
			.build();
	}
	
	private ClientRegistration inruptClientRegistration() {
		return ClientRegistration.withRegistrationId("inrupt")
			.clientId("a4bb2a00f3c4fbbf60c7127aaf75ecbf")
			.clientSecret("c6fcc08187f2f5b4d927d361dca52d4a")
			.clientAuthenticationMethod(ClientAuthenticationMethod.BASIC)
			.authorizationGrantType(AuthorizationGrantType.AUTHORIZATION_CODE)
			.redirectUriTemplate("{baseUrl}/login/oauth2/code/{registrationId}")
			.scope("openid", "read", "write", "append", "control")
			.authorizationUri("https://inrupt.net/authorize")
			.tokenUri("https://inrupt.net/token")
			.userNameAttributeName(IdTokenClaimNames.SUB)
			.jwkSetUri("https://inrupt.net/jwks")
			.clientName("inrupt")
			.build();
	}
	
}
