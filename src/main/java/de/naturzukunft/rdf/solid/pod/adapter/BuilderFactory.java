package de.naturzukunft.rdf.solid.pod.adapter;

import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;

@Component
public class BuilderFactory {

	private DPopProvider dPopProvider;
	private WebClient webClient;

	public BuilderFactory(DPopProvider dPopProvider, WebClient webClient) {
		this.dPopProvider = dPopProvider;
		this.webClient = webClient;
	}
	
	public ResourceBuilder createResourceBuilder() {
		return new DefaultResourceBuilder(dPopProvider, webClient);
	}

	public ResourceBuilder createResourceBuilder(String clientId, String accessToken) {
		return new DefaultResourceBuilder(dPopProvider, webClient, clientId, accessToken);
	}

	public ResourceBuilder createBasicContainerBuilder() {
		return new BasicContainerBuilder(dPopProvider, webClient);
	}

	public ResourceBuilder createBasicContainerBuilder(String clientId, String accessToken) {
		return new BasicContainerBuilder(dPopProvider, webClient, clientId, accessToken);
	}
}
